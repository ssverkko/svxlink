###############################################################################
#
# ReflectorLogic event handlers
#
###############################################################################

#
# This is the namespace in which all functions below will exist. The name
# must match the corresponding section "[ReflectorLogic]" in the configuration
# file. The name may be changed but it must be changed in both places.
#
namespace eval ReflectorLogic {

# The currently selected TG. Variable set from application.
variable selected_tg 0

# The previously selected TG. Variable set from application.
variable previous_tg 0

# Timestamp for previous TG announcement
variable prev_announce_time 0

# The previously announced TG
variable prev_announce_tg 0

# The minimum time between announcements of the same TG.
# Change through ANNOUNCE_REMOTE_MIN_INTERVAL config variable.
variable announce_remote_min_interval 0

# This variable will be set to 1 if the QSY pending feature ("QSY on squelch
# activity") is active. See configuration variable QSY_PENDING_TIMEOUT.
variable qsy_pending_active 0

#
# Checking to see if this is the correct logic core
#
if {$logic_name != [namespace tail [namespace current]]} {
  return;
}


#
# Executed when a TG selection has timed out
#
#   new_tg -- Always 0
#   old_tg -- The talk group that was active
#
proc tg_selection_timeout {new_tg old_tg} {
  #puts "### tg_selection_timeout"
  if {$old_tg != 0} {

    # Set CW parameters
    CW::setAmplitude -18
    CW::setCpm 120

    # Wake sleeping receivers
    playSilence 1000
    
    # Play 'Thank You'
    CW::play "TU"
    playSilence 300

    # Play SK without character spacing
    CW::play "S"
    playSilence 50
    CW::play "K"

    playSilence 200
  }
}



#
# Executed on talker stop
#
#   tg        -- The talk group
#   callsign  -- The callsign of the talker node
#
proc talker_stop {tg callsign} {
  variable selected_tg
  variable ::Logic::CFG_CALLSIGN
  #puts "### Talker stop on TG #$tg: $callsign"
  if {($tg == $selected_tg) && ($callsign != $::Logic::CFG_CALLSIGN)} {

    # Set CW parameters
    CW::setAmplitude -18
    CW::setCpm 120

    playSilence 300

    set fields [split $callsign "-"]
    if {[string length [lindex $fields 1]] > 1} {
        CW::play [lindex $fields 1]
    } else {
        #CW::play $callsign
        if {($tg == $selected_tg) && ($callsign != $::Logic::CFG_CALLSIGN)} {
            playSilence 100
            playTone 440 200 50
            playTone 659 200 50
            playTone 880 200 50
        }
    }

    playSilence 200
  }
}


# end of namespace
}


#
# This file has not been truncated
#
